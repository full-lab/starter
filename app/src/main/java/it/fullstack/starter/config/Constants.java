package it.fullstack.starter.config;

import java.time.format.DateTimeFormatter;

/**
 * Constants for Spring Security authorities.
 */
public final class Constants {

  public static final String SYSTEM_ACCOUNT = "system"; 
  
  // Ruoli //
  public final static String RESPONSABILE = "ROLE_CNT_RESPONSABILE";
  public final static String OPERATORE_DG = "ROLE_CNT_OPERATORE_DG";
  public final static String OPERATORE_DR = "ROLE_CNT_OPERATORE_DR";
  
  /** Constant <code>SPRING_PROFILE_DEVELOPMENT="dev"</code> */
  public static final String SPRING_PROFILE_DEVELOPMENT = "dev";
  
  public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");
  public static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

  private Constants() {
  }

}

