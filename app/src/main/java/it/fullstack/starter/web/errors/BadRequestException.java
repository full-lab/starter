package it.fullstack.starter.web.errors;

import lombok.Getter;

@Getter
public class BadRequestException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private final String errorCode;

	public BadRequestException(String errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}

	public BadRequestException(String errorCode, String format, Object... args) {
		this(errorCode, String.format(format, args));
	} 

}
