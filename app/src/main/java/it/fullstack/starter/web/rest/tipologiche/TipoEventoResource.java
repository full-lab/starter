package it.fullstack.starter.web.rest.tipologiche;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fullstack.starter.domain.tipologiche.TipoEvento;
import it.fullstack.starter.repository.TipoEventoRepository;

@RestController
@RequestMapping("/api/tipologiche/tipo-evento")
public class TipoEventoResource extends AbstractTipologicaResource<TipoEvento> {

  public TipoEventoResource(TipoEventoRepository repository) {
    super("Aggregato", "tipo-evento", repository);
  }

}
