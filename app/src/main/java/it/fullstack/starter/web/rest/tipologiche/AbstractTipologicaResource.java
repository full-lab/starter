package it.fullstack.starter.web.rest.tipologiche;

import static java.lang.String.format;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import it.fullstack.sdk.core.exception.domain.NotFoundException;
import it.fullstack.starter.domain.tipologiche.AbstractTipologica;
import it.fullstack.starter.web.errors.BadRequestException;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AbstractTipologicaResource<T extends AbstractTipologica> {

  private final String entityName;
  private final String path;
  private final JpaRepository<T, Long> repository;
  
  public AbstractTipologicaResource(String entityName, String path, JpaRepository<T, Long> repository) {
    this.entityName = entityName;
    this.path = path;
    this.repository = repository;
  }

  @GetMapping
  public ResponseEntity<List<T>> findAll() {
    log.debug("REST request to get all {}", entityName);

    List<T> result = repository.findAll();
    return ResponseEntity.ok().body(result);
  }

  @PostMapping
  public ResponseEntity<T> create(@Valid @RequestBody T entity) throws URISyntaxException {
    log.debug("REST request to save {}: {}", entityName, entity);
    if (entity.getId() != null) {
      throw new BadRequestException("Invalid id", entityName, "idnull");
    }

    T result = repository.save(entity);
    return ResponseEntity
        .created(new URI(format("%s/%s", path, result.getId())))
        .body(result);
  }

  @PutMapping("/{id}")
  public ResponseEntity<T> update(@PathVariable Long id, @Valid @RequestBody T entity) throws URISyntaxException {
    log.debug("REST request to update {}: {}", entityName, entity);
    
    if (!Objects.equals(id, entity.getId())) {
      throw new BadRequestException("Invalid id", entityName, "idinvalid");
    }

    T result = repository.save(entity);
    return ResponseEntity
        .ok()
        .body(result);
  }

  @DeleteMapping("/{id}")
  @SuppressWarnings("unchecked")
  public ResponseEntity<Void> delete(@PathVariable Long id) {
    log.debug("REST request to delete {}: {}", entityName, id);

    repository.findById(id)
    .map(T::close)
    .ifPresentOrElse(
        t -> repository.save((T) t), 
        () -> new NotFoundException("%s not found: %s", entityName, id)
    );

    return ResponseEntity.ok().build();
  }
}
