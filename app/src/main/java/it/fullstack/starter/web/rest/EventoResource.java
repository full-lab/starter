package it.fullstack.starter.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fullstack.sdk.core.web.util.PaginationUtil;
import it.fullstack.starter.domain.Evento;
import it.fullstack.starter.services.EventoService;
import it.fullstack.starter.services.dto.CreateEventoDTO;
import it.fullstack.starter.services.dto.UpdateEventoDTO;
import it.fullstack.starter.services.query.EventoCriteria;
import it.fullstack.starter.services.query.EventoQueryService;
import it.fullstack.starter.web.errors.BadRequestException;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/evento")
@AllArgsConstructor

public class EventoResource {

  private final EventoService service;
  private final EventoQueryService queryService;

  private static final String ENTITY_NAME = "evento";
  
  @GetMapping("{id}")
  public ResponseEntity<Evento> findOne(@PathVariable Long id) {
    log.debug("REST request to get one {}", ENTITY_NAME);

    return ResponseEntity.of(service.findOne(id));
  }
 
  @GetMapping
  public ResponseEntity<List<Evento>> findByCriteria(EventoCriteria criteria, Pageable pageable) {
    log.debug("REST request to get {} by Criteria:{} and Page:{}", ENTITY_NAME, criteria, pageable);
    
    Page<Evento> page = queryService.findByCriteria(criteria, pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/evento");
    
    return ResponseEntity.ok()
        .headers(headers)
        .body(page.getContent());
  }
  
  @PostMapping
  public ResponseEntity<Evento> create(@Valid @RequestBody CreateEventoDTO dto) throws URISyntaxException {
    log.debug("REST request to save {}: {}", ENTITY_NAME, dto);

    Evento result = service.create(dto);
    return ResponseEntity
        .created(new URI("/api/evento/" + result.getId()))
        .body(result);
  }

  @PutMapping("/{id}")
  public ResponseEntity<Evento> update(@PathVariable Long id, @Valid @RequestBody UpdateEventoDTO dto) throws URISyntaxException {
    log.debug("REST request to update {}: {}", ENTITY_NAME, dto);
    
    if (dto.getId() == null) {
      throw new BadRequestException("Invalid id", ENTITY_NAME, "idnull");
    }
    if (!Objects.equals(id, dto.getId())) {
      throw new BadRequestException("Invalid id", ENTITY_NAME, "idinvalid");
    }

    Evento result = service.update(dto);
    return ResponseEntity
        .ok()
        .body(result);
  }
  
  @DeleteMapping("/{id}")
  public ResponseEntity<Void> delete(@PathVariable Long id) {
    log.debug("REST request to delete {}: {}", ENTITY_NAME, id);
    
    service.delete(id);

    return ResponseEntity
        .ok()
        .build();
  }
}
