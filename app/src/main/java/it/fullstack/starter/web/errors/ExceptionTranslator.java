package it.fullstack.starter.web.errors;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import jakarta.validation.ConstraintViolationException;


@ControllerAdvice
public class ExceptionTranslator {
  
  @ExceptionHandler
  public ResponseEntity<String> handleBadRequest(BadRequestException ex, WebRequest request) {
    return ResponseEntity.badRequest().body(ex.getMessage());
  }
 
  @ExceptionHandler
  public ResponseEntity<Void> handleBadRequest(jakarta.persistence.EntityNotFoundException ex, WebRequest request) {
    return ResponseEntity
        .notFound()
        .header("Message", ex.getMessage())
        .build();
  }
  
  @ExceptionHandler({ ConstraintViolationException.class })
  public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
      List<String> errors = ex.getConstraintViolations().stream()
        .map(v -> v.getPropertyPath() + ": " + v.getMessage())
        .collect(Collectors.toList());

      ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), errors);
      return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
  }

}
