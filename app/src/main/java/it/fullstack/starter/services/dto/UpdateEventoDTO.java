package it.fullstack.starter.services.dto;

import it.fullstack.starter.domain.tipologiche.TipoEvento;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class UpdateEventoDTO {
  
  @NotNull
  private Long id;

  @NotNull
  private TipoEvento tipo;

  @NotNull
  private String messaggio;
}
