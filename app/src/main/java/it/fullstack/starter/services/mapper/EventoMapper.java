package it.fullstack.starter.services.mapper;

import it.fullstack.starter.domain.Evento;
import it.fullstack.starter.services.dto.CreateEventoDTO;
import it.fullstack.starter.services.dto.UpdateEventoDTO;

public interface EventoMapper {

  public static Evento toEntity(CreateEventoDTO dto) {
    return new Evento()
        .messaggio(dto.getMessaggio());
  }
  
  public static Evento toEntity(Evento evento, UpdateEventoDTO dto) {
    return evento
        .messaggio(dto.getMessaggio())
        .tipo(dto.getTipo());
  }

}
