package it.fullstack.starter.services.query;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fullstack.sdk.core.service.QueryService;
import it.fullstack.starter.domain.Evento;
import it.fullstack.starter.domain.Evento_;
import it.fullstack.starter.domain.tipologiche.TipoEvento_;
import it.fullstack.starter.repository.EventoRepository;
import jakarta.persistence.criteria.JoinType;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
@Transactional(readOnly = true)
@AllArgsConstructor
public class EventoQueryService extends QueryService<Evento> {

  private final EventoRepository eventoRepository;

 
  @Transactional(readOnly = true)
  public List<Evento> findByCriteria(EventoCriteria criteria) {
    log.debug("find by criteria : {}", criteria);

    return eventoRepository.findAll(createSpecification(criteria));
  }

  @Transactional(readOnly = true)
  public Page<Evento> findByCriteria(EventoCriteria criteria, Pageable page) {
    log.debug("find by criteria : {}, page: {}", criteria, page);

    return eventoRepository.findAll(createSpecification(criteria), page);
  }

  private Specification<Evento> createSpecification(EventoCriteria criteria) {
      Specification<Evento> specification = Specification.where(null);
    
      if (criteria != null) {
        if (criteria.getQ() != null) {
            Specification<Evento> messaggio =  like(criteria.getQ(), Evento_.messaggio);
            specification = specification.and(messaggio);
        }
        
      if (criteria.getId() != null) {
        specification = specification.and(buildSpecification(criteria.getId(), Evento_.id));
      }
      if (criteria.getMessaggio() != null) {
        specification = specification.and(buildStringSpecification(criteria.getMessaggio(), Evento_.messaggio));
      }
      if (criteria.getRicevuto() != null) {
        specification = specification.and(buildRangeSpecification(criteria.getRicevuto(), Evento_.ricevuto));
      }
      if (criteria.getTipo() != null) {
        specification = specification.and(buildSpecification(criteria.getTipo(), 
            root -> root.join(Evento_.tipo, JoinType.LEFT).get(TipoEvento_.id)));
      }      
    }
      
    return specification;
  }

}
