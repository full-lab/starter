package it.fullstack.starter.services.query;

import java.io.Serializable;

import it.fullstack.sdk.core.service.filter.LocalDateTimeFilter;
import it.fullstack.sdk.core.service.filter.LongFilter;
import it.fullstack.sdk.core.service.filter.StringFilter;
import lombok.Data;

@Data
public class EventoCriteria implements Serializable {
  private static final long serialVersionUID = 1L;

  private String q;
   
  private LongFilter id;
  private StringFilter messaggio;
  private LongFilter tipo;
  private LocalDateTimeFilter ricevuto;
}
