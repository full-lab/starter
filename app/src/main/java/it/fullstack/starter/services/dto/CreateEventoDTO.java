package it.fullstack.starter.services.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CreateEventoDTO {
  @NotNull
  private String messaggio;
}
