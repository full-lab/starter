package it.fullstack.starter.domain.enumeration;

public enum GradoProcessuale {
  
  I("I grado"),
  II("II grado"),
  CASSAZIONE("Cassazione");

  public String descrizione;

  GradoProcessuale(String descrizione) {
    this.descrizione = descrizione;
  }
  
  @Override
  public String toString() {
    return descrizione;
  }

}
