package it.fullstack.starter.domain.tipologiche;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@MappedSuperclass
@EqualsAndHashCode(of = "id")
public abstract class AbstractTipologica {
 
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  protected Long id;

  @NotNull
  @Column(nullable = false, unique = true)
  private String descrizione;

  private LocalDateTime dataChiusura;

  public AbstractTipologica id(Long id) {
    this.id = id;
    return this;
  }

  public AbstractTipologica descrizione(String descrizione) {
    this.descrizione = descrizione;
    return this;
  }

  public AbstractTipologica dataChiusura(LocalDateTime dataChiusura) {
    this.dataChiusura = dataChiusura;
    return this;
  }

  public AbstractTipologica close() {
    this.dataChiusura = LocalDateTime.now();
    return this;
  }

  public boolean isClosed() {
    return dataChiusura != null;
  }
}
