package it.fullstack.starter.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import it.fullstack.starter.domain.tipologiche.TipoEvento;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
public class Evento extends AbstractAuditingEntity implements Serializable {
 
  private static final long serialVersionUID = 1L;

  @Id 
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotNull
  @ManyToOne(optional = false)
  private TipoEvento tipo;
  
  private String messaggio;

  @NotNull
  private LocalDateTime ricevuto;

  public Evento id(Long id) {
    this.id = id;
    return this;
  }

  public Evento tipo(TipoEvento tipo) {
    this.tipo = tipo;
    return this;
  }
  
  public Evento messaggio(String messaggio) {
    this.messaggio = messaggio;
    return this;
  }
  
  public Evento messaggio(String messaggio, Object... args) {
    return messaggio(String.format(messaggio, args));
  }

  public Evento ricevuto(LocalDateTime ricevuto) {
    this.ricevuto = ricevuto;
    return this;
  }

}
