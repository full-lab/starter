package it.fullstack.sdk.core;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.CorsRegistration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * Configurazione Cors
 */

@Data
@Slf4j
@Component
@ConditionalOnProperty(prefix = "cors", name= "enabled", matchIfMissing = false)
@ConfigurationProperties(prefix = "cors")
public class SdkCorsConfiguration {

	/**
	 * Enable Cors
	 */
	private boolean enabled;

	/**
	 * Allowed Origins array of URLs
	 * 
	 * example: http://localhost:4200
	 * 
	 * ref:
	 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Origin
	 */
	private String[] allowedOrigins = { "*" };

	/**
	 * Allowed Origins Patterns wildcards to specify patterns if values are provided
	 * then allowedOrigins is not used
	 * 
	 * example: *.agenziademanio.it, *.demaniodg.it
	 */
	private String[] allowedOriginsPatterns = {};

	/**
	 * Allowed Methods array of strings example: GET, POST, HEAD, OPTIONS
	 * 
	 * ref:
	 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Methods
	 */
	private String[] allowedMethods = { "*" };

	/**
	 * Allowed Headers array of strings example: Authorization, Content-Type
	 * 
	 * ref:
	 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Headers
	 */
	private String[] allowedHeaders = { "*" };

	/**
	 * Exposed Headers array of strings
	 * 
	 * ref:
	 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Expose-Headers
	 */
	private String[] exposedHeaders = { "*" };

	/**
	 * Max Age
	 * 
	 * ref:
	 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Max-Age
	 */
	private long maxAge = 5;

	/**
	 * Allow Credentials
	 * 
	 * ref:
	 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Credentials
	 */
	private boolean allowCredentials = false;

	@Bean
	WebMvcConfigurer corsMappingConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				log.debug("Enabling Cors WebMvcConfigurer");
				CorsRegistration reg = registry.addMapping("/**");
				reg
				  .maxAge(maxAge)
				  .allowedOrigins(allowedOrigins)
				  .allowedMethods(allowedMethods)
					.allowedHeaders(allowedHeaders)
					.allowCredentials(allowCredentials);

				if (allowedOriginsPatterns != null && allowedOriginsPatterns.length > 0) {
					reg.allowedOriginPatterns(allowedOriginsPatterns);
				}

				if (exposedHeaders != null && exposedHeaders.length > 0) {
					reg.exposedHeaders(exposedHeaders);
				}
			}
		};
	}
}
