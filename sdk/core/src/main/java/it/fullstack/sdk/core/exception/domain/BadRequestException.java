package it.fullstack.sdk.core.exception.domain;

public class BadRequestException extends AbstractDomainException {
    private static final long serialVersionUID = 1L;
  
    public static final String CODE = "400_000001";

    public BadRequestException(String message) {
        super(CODE, message);
    }

    public BadRequestException(String format, Object... args) {
        super(CODE, format, args);
    }
}
