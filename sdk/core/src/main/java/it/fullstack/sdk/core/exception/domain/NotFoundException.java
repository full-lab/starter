package it.fullstack.sdk.core.exception.domain;

public class NotFoundException extends AbstractDomainException {
    private static final long serialVersionUID = 1L;
    public static final String CODE = "404_000001";
    public NotFoundException(String message) {
        super(CODE, message);
    }

    public NotFoundException(String format, Object... args) {
        super(CODE, format, args);
    }
}
