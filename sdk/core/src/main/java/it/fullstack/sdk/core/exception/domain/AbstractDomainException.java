package it.fullstack.sdk.core.exception.domain;

public class AbstractDomainException extends RuntimeException implements DomainExceptionInterface {
	private static final long serialVersionUID = 1L;
	private final String code;

	public String getCode() {
		return code;
	}

	public AbstractDomainException(String errorCode, String message) {
		super(message);
		this.code = errorCode;
	}

	public AbstractDomainException(String errorCode, String format, Object... args) {
		this(errorCode, String.format(format, args));
	}
}
