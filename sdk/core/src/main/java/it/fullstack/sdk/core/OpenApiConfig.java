package it.fullstack.sdk.core;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;
import lombok.Data;

/**
 * Configurazione OpenAPI
 */

@Data
@Component
@ConditionalOnProperty(prefix = "openapi", name = "enabled", matchIfMissing = false)
@ConfigurationProperties(prefix = "openapi")
public class OpenApiConfig {

  /**
   * Enable OpenAPI
   */
  private boolean enabled;

  /**
   * If true add lock to APIs by default
   */
  private boolean locked = true;

  /**
   * Titolo OpenAPI
   */
  private String title;

  /**
   * Versione OpenAPI
   */
  private String version;

  /**
   * Descrizione OpenAPI
   */
  private String description;

  /**
   * Lista di server OpenAPI. Ogni server ha una url e una descrizione
   */
  private List<Server> servers;

  @Bean
  OpenAPI openAPI() {
    OpenAPI opDefinition = new OpenAPI()
        .servers(servers)
        .info(new Info().title(title).version(version).description(description));
    if (locked) {
      opDefinition
        .addSecurityItem(new SecurityRequirement().addList("Bearer Token"))
        .components(new Components().addSecuritySchemes("Bearer Token", new SecurityScheme().type(SecurityScheme.Type.HTTP).scheme("bearer").bearerFormat("JWT")));
    }
    return opDefinition;
  }

}
