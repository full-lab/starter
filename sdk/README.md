# SDK - 3.0.X

## Versioni e Compatibilità

Questa versione è compatibile con le versioni 3.0.x di Spring Boot


E' possibile importare ed utilizzare i singoli moduli necessari per la applicazione, o, in alternativa importare la seguente dipendenza per includere tutti i moduli.

```
<dependency>
    <groupId>it.fullstack.sdk</groupId>
    <artifactId>sdk-core</artifactId>
    <version>3.0.x</version>
</dependency>
```

## Configurare i plugin/s

La configurazione del Plugin avviene nel application.properties o application.yml;

Versione application.properties

```
spring.application.name=[NOME_APPLICAZIONE]
spring.profiles.include=sdk-local

```

Versione application.yml

```
spring:
  application:
    name: [NOME_APPLICAZIONE]
  profiles:
    include:
    - sdk-local
    #- sdk-sviluppo
    #- sdk-test
    #- sdk-validazione
    #- sdk-produzione
```
